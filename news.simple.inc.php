<?php

function render_digest( $title, $link ) {
	$out = <<<DIGEST
	<tr>
		<td align="left" valign="middle" style="padding-top: 8px; padding-bottom: 5px">
			<a href="{$link}" target="_blank">
				<img src="https://investinfra.ru/frontend/images/tn-banner/banner-digest.png" width="720" height="128" alt="{$title}" border="0">
			</a>
		</td>
	</tr>
DIGEST;

	return $out;
}


function render_news_row( $title, $link ) {
	$out = <<<NEWSROW
	<tr>
		<td align="left" valign="middle" style="padding-top: 8px; padding-bottom: 8px">
			<a href="{$link}" style="font-size: 16px; font-family: Arial, sans-serif; color: #444444; font-weight: 600; text-decoration: none" target="_blank">{$title}</a>
		</td>
	</tr>
NEWSROW;

return $out;
}


?>
