
$(document).ready(function() {
	$( '.flags input[type="number"]' ).each(function() {
		var entry_no=$(this).attr('name').match( /\d*$/ )[0]; //выдергиваем номер шпигеля
		$(this).attr( 'data', entry_no );
		var placeholder=$('#spiegel-text-'+entry_no);
		
		placeholder.text( placeholder.text().replace(/^\s*(.*\S+)\s*$/,"$1") );
		placeholder.attr( 'text', placeholder.text() );//резервируем содержимое в text
		placeholder.attr( 'data', $(this).attr('value') );//резервируем первоначальное количество слов
		
	}
	); 
	
	$( '.flags input[type="number"]' ).change(function() {
		//var words_count=$(this).attr('value');
		
		var entry_no = $(this).attr('name').match( /\d*$/ )[0]; //выдергиваем номер шпигеля
		var placeholder = $('#spiegel-text-'+entry_no);
		var placeholder_start_text =  placeholder.attr('text');
		
		var diff = + placeholder.attr('data') - $(this).val();
		
		
	//	placeholder.text( placeholder.text().replace(/\.{3,}\s*$/,"") );
		//var re=
		//var re =  '/(\s+\S+\s*){'+''+'}$/';
		//console.log(diff, placeholder_start_text );
		
		for (var i=0; i<diff; i++)
		{
			placeholder_start_text=placeholder_start_text.replace( /(\s+\S+\s*)$/ ,"" );
			
		}
		placeholder.text( placeholder_start_text );
	}
	);
	
	$('.flags').has('option[selected]').addClass('foreign');
	
	$('.dashboard-button-save').on('click', function() {
		
		var data_to_save = {};
		
		$('.title').each(function (i, item) {
			var	current_link = $(item).find('.link').text() ;
				
			var current_key = current_link.match( /([^/]+)\.html$/ )[1];
			
			var current_words_count = ($(item).find('.words-count').val() !== undefined) ? $(item).find('.words-count').val() : -1;
			
			var current_news_type = $(item).find('.news-type').val();
			
			//console.log( $(item).find('.words-count').val() );
			
			data_to_save[current_key] = { news_type :  current_news_type, words_count : current_words_count};
				
			}
		);
			
		$.get(
			"/loadsave.php",
			{
				action: "save",
				data: data_to_save
			},
			
			function (response)
			{
			  // Здесь мы получаем данные, отправленные сервером и выводим их на экран.
			  alert("Данные рассылки сохранены");
			  //console.log(response);
			}
		);
		
	}); 
	
	$('.dashboard-button-load').on('click', function() {
		
		
		//console.log( data_to_save );
		
		$.get(
			"/loadsave.php",
			{
				action: "load",
				data: ""
			},
			
			function (response)
			{
			  
				data_loaded = JSON.parse(response);
			  
				$('.title').each(function (i, item) {
					
					var	current_link = $(item).find('.link').text() ;
					
					var current_key = current_link.match( /([^/]+)\.html$/ )[1];
					
					if (data_loaded[current_key] !== undefined)
					{
						var current_news_type = data_loaded[current_key]['news_type'];
						

						console.log( current_news_type, ">", $(item).find('select.news-type option[name='+current_news_type+']').text() );
						
						$(item).find('select.news-type option[name='+current_news_type+']').prop('selected', true);
						$(item).find('select.news-type option[name='+current_news_type+']').attr("selected", "selected");
						
						var current_words_count = data_loaded[current_key]['words_count'];
						
						if (+ current_words_count > -1) {
							console.log (i," наличие сокращения ",$(item).find('.words-count').length); 
							if ($(item).find('.words-count').length == 0)
							{
								s = '<div class="flags">Количество слов: <input type="number" class="words-count" name="words-count{i}" value="11" min="1"  size="2" data="{i}"></div>'
								s = s.replace(/\{i\}/g, i);
								$(item).find('.flags').after( $(s) );
							}
							$(item).find('.words-count').val(current_words_count);
						}	
					}
				
				});
				
				$('.flags').removeClass('foreign');
				$('.flags').has('option[selected]').addClass('foreign');
			}
		);
		
	}); 
	
	
});

