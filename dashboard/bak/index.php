<!DOCTYPE html>
	<?php 
	$rss = "http://www.investinfra.ru/rss/";
    $content = file_get_contents($rss);
	$banners_content = file_get_contents("banners.xml");
	$banners_xml = new SimpleXMLElement($banners_content);	
	?> 
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="none"/>
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="">

	<title>Генератор рассылок</title>
	
	<link rel="stylesheet" href="/css/styles.css?<?php echo rand(1, 9999);?>"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>

</head>
<body>


<div class="column column-form">
	<form action="index.php" method="post">
	
	
	<?php
	
	$xml = new SimpleXMLElement($content);
	 
	$i=0;
	$spiegels=[];
	$images=[];
	$removed_words_count=[];
	
	$images['http://www.investinfra.ru/frontend/images/icons/facebook.png']=1;
	$images['http://www.investinfra.ru/frontend/images/icons/twitter.png']=1;
	$images['http://www.investinfra.ru/frontend/images/icons/youtube.png']=1;
	$images['http://www.investinfra.ru/frontend/images/icons/rss.png']=1;
	$images['http://www.investinfra.ru/frontend/images/1x1.png']=1;

	
	$alldays=isset($_POST['all-days-enabled']) ? 1 : 0;
	$makefile=isset($_POST['make-file']) ? 1 : 0;
	
	
	foreach($xml->xpath('//item') as $item){
		if ( date_parse_from_format("D, d M Y", $item->pubDate)["day"]==date("d") || $alldays)
		{
		echo '<div class="title">';
			echo '<div class="item ">'.$item->title.'</div>';
					
			echo '<div class="flags ">';
			
			$current_news_type=isset( $_POST['flags'.$i] ) ? $_POST['flags'.$i] : "0"; //в $current_news_type записываем выбор селекта для текущей новости
			
			if ($current_news_type != "0") {
				$spiegels[$current_news_type]=$i;
			}
			
			//echo $s;
				echo '<select name="flags'.$i.'">';
					
					echo '<option value="0">Обычная новость</option>';
					
					if ($current_news_type == "1") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="1"'.$sel.'>Русский главный</option>';
					
					if ($current_news_type == "2") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="2"'.$sel.'>Русский - 1</option>';
					
					if ($current_news_type == "3") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="3"'.$sel.'>Русский - 2</option>';
					
					if ($current_news_type == "4") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="4"'.$sel.'>Русский - 3</option>';
					
					if ($current_news_type == "5") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="5"'.$sel.'>Иностранный главный</option>';
					
					if ($current_news_type == "6") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="6"'.$sel.'>Иностранный - 1</option>';
					
					if ($current_news_type == "7") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="7"'.$sel.'>Иностранный - 2</option>';
					
					if ($current_news_type == "8") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="8"'.$sel.'>Иностранный - 3</option>';
					
					if ($current_news_type == "404") {$sel=" selected";} else {$sel=$current_news_type;}
					echo '<option value="404"'.$sel.'>Скрыть</option>';
				echo '</select>';
			
			echo '</div>';
			
			if ( (int)$current_news_type > 0 && (int)$current_news_type <= 8 ) {//если мы в шпигеле
					
					if ( (int)$current_news_type == 1 || (int)$current_news_type == 5 ) { 
						$full_text=explode("\n",$item->{'full-text'})[0];
						$words = preg_split("/[\s]+/", $full_text); 
					} else {
						$words = preg_split("/[\s]+/", $item->title);		
					}
					
					$words_in_spiegel=count($words);

					if (isset( $_POST['words-count'.$i] ) && $_POST['words-count'.$i] < $words_in_spiegel) {
						$removed_words_count[$i]=(int)$words_in_spiegel-(int)$_POST['words-count'.$i];
						$words_count=$_POST['words-count'.$i];
						
					} else {
						$words_count = $words_in_spiegel;
					}
					
					
					echo '<div class="flags ">Количество слов: <input type="number" name="words-count'.$i.'" value="'.$words_count.'" min="1" max="'.$words_in_spiegel.'" size="2"></div>';
				}	
			
		echo '</div>';
		}
		$i++;
	}
	//print_r ($removed_words_count);
	//print_r($spiegels);
	?>
	
	
	<div class="dashboard">
		
	
		<input type="checkbox" id="all-days-enabled" name="all-days-enabled" <?php echo isset($_POST['all-days-enabled']) ? ' checked' : ''; ?> >
		
		<label for="all-days-enabled" class="dashboard-label" style="padding-right: 20px"  >За все дни</label>

		<label for="banners" class="dashboard-label">Баннеры: </label>
		<select name="banners" id="banners" class="dashboard-select">
			<?php
					 
			
			$i=0;		
			
			foreach($banners_xml->xpath('//banner') as $banner){
				if ($_POST['banners']=="".$i) {$sel=" selected";} else {$sel="";}
				echo '<option value="'.$i.'"'.$sel.'>'.$banner->title.'</option>';
				$i++;
			}/**/
			?>
		</select>
		
		<select name="smallbanners" id="smallbanners" class="dashboard-select">
			<?php
					 
			
			$i=0;		
			
			foreach($banners_xml->xpath('//smallbanner') as $banner){
				if ($_POST['smallbanners']=="".$i) {$sel=" selected";} else {$sel="";}
				echo '<option value="'.$i.'"'.$sel.'>'.$banner->title.'</option>';
				$i++;
			}/**/
			?>
		</select>
		
		<input type="checkbox" id="make-file" name="make-file" <?php echo isset($_POST['make-file']) ? ' checked' : ''; ?> >
		<label for="make-file" class="dashboard-label" style="padding-right: 20px"  >Сформировать файл</label>

		<button class="dashboard-button" type="submit">Обновить</button>
	</div>
</form>
	
</div>	
<div class="column column-mail">

<?php include 'title.inc.php'; ?>
<?php include 'topdate.inc.php'; ?>

<?php include_once 'heading.inc.php'; ?>
<?php include_once 'news.single.inc.php'; ?>
<?php include_once 'banner.small.inc.php'; ?>

<?php function newsletter($xml, $banners_xml, $spiegels) { ?>



<?php 
	ob_start(); 
	global $alldays; 
	global $images; 
	global $removed_words_count;
	global $banners_xml;
	
	$rus_moth = [
	  'января',
	  'февраля',
	  'марта',
	  'апреля',
	  'мая',
	  'июня',
	  'июля',
	  'августа',
	  'сентября',
	  'октября',
	  'ноября',
	  'декабря'
	];
	
	$date_month  = date("d")." ".$rus_moth[(int)date("m")-1];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<section>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<title>Ежедневная лента новостей информационного агенства ИНВЕСТИНФРА от <?php echo $date_month;?> 2018 года</title>

	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
</head>

<style>
	html * {max-height:1000000px;}
</style>

<article>  
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
<!-- главная обертка -->
<tbody>
<tr>
	<td valign="middle">
	
		<!-- шапка красная -->
		
		<?php title() //title.inc.php ?>
		
		<!-- шапка белая -->
		
		<?php topdate() //topdate.inc.php ?>

		
		<!-- русский шпигель заголовок -->
		
		<?php heading("В РОССИИ",0) //heading.inc.php ?>

		
		<!-- русский шпигель главный -->
		<?php 			
		if (isset($spiegels['1'])) {
			$doc_address=$xml->xpath('//item')[$spiegels['1']]->link;
			$doc_content = file_get_contents($doc_address);
			
			$preffix='<meta property="og:image" content="';
			$affix='">';
			
			$doc_content=substr( $doc_content, strpos($doc_content,$preffix)+strlen($preffix), strlen($doc_content) );	
			$doc_content=substr( $doc_content, 0 , strpos($doc_content,$affix) );
			
			
						$current_image=$doc_content;	
						$current_title=$xml->xpath('//item')[$spiegels['1']]->title;		
				}else {
						$current_image="https://www.investinfra.ru/frontend/images/news/404.png";
						$current_title="Главный русский шпигель не выбран";
						$doc_address="";
						$doc_content="Not defined";
			}		
			$images[$current_image]=1;
		?>
		
		<table width="720" align="center" border="0" cellspacing="0" cellpadding="0"  bgcolor="EEEEEE">
		<tbody>
		<tr>
			<td width="240" valign="top" align="center">
				<table width="220" border="0" cellspacing="0" cellpadding="0">
					<tr>	
						<td width="220">
						
							<!-- главный фотография -->
						<img style="display: block" src="<?php echo $current_image ?>" width="220" alt="<?php echo $current_title; ?>" border="0">
						
						</td>
					</tr>
				</table>		
			</td>
			<td width="480" align="center" valign="top">
				<table width="460" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" valign="top">
							<!-- главный заголовок -->
							<div style="font-size: 16px !important; line-height: 18px !important;  padding-bottom: 4px">
								<a href="<?php echo $doc_address ?>" style="font-weight: bold; font-family: Arial, sans-serif; color: #444444; text-decoration: none; max-height: 10000px" target="_blank">
								<?php echo $current_title; ?>
								</a>
							</div>
							
							<!-- главный лид -->
							
							<div style="font-size: 14px !important; line-height: 17px !important">
								
								<?php
									if (isset($spiegels['1'])) {
										$fulltext=$xml->xpath('//item')[$spiegels['1']]->{'full-text'};
										$fulltext=explode("\n", $fulltext)[0];
										
										if ( isset($removed_words_count[$spiegels['1']]) ) {
											//echo '<p>Кол-во слов сократить на '. $removed_words_count[$spiegels['1']].'</p>' ;//
											for ($j=0; $j<(int)$removed_words_count[$spiegels['1']]; $j++) { 
											$fulltext=preg_replace('=\s\S+$=', '', $fulltext); 
											//echo '|';
											}
										}
									}else {
										$fulltext="Главный русский шпигель не выбран Главный русский шпигель  не выбран Главный русский шпигель  не выбран Главный русский шпигель  не выбран";
									}
																		
									$fulltext=htmlspecialchars($fulltext);
									$fulltext=preg_replace('=.\s$=', '', $fulltext);

									
								?> 
								<a id="spiegel-text-<?php echo $spiegels['1'] ?>" href="<?php echo $doc_address ?>" text="<?php echo $fulltext; ?>" style="font-family: Arial, sans-serif; color: #444444; text-decoration: none; max-height: 10000px" target="_blank"> <?php echo $fulltext; ?> </a>...
							</div>
						</td>
					</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td height="10" colspan="2">
				<img src="http://www.investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" style="display: block">
			</td>
		</tr>
		</tbody>
		</table>
		<!-- / русский шпигель главный -->
				
		<!-- русский шпигель 3 в ряд -->
		<table width="720" align="center" border="0" cellspacing="0" cellpadding="10" bgcolor="EEEEEE">
		<tbody>
		<tr>
			<td width="240" valign="top">				
				<!-- одиночная новость  -->
				<?php //echo isset($spiegels['2']) ? $spiegels['2'] : "empty" 	 ?>
					
				<?php news_single( isset($spiegels['2']) ? $spiegels['2'] : "empty" )	//news.single.inc.php ?>	
				
			</td>
			<td width="240" valign="top">
				<!-- одиночная новость  -->
			
				<?php news_single( isset($spiegels['3']) ? $spiegels['3'] : "empty" ) //news.single.inc.php ?>	
				
				
			</td>
			<td width="240" valign="top">		
				<!-- одиночная новость -->
				
				<?php news_single( isset($spiegels['4']) ? $spiegels['4'] : "empty" ) //news.single.inc.php ?>	
				
			</td>			
		</tr>
		</tbody>
		</table>
		<!-- /русский шпигель 3 в ряд -->
		
		<!-- spiegel end -->
		
		<!-- simple lines -->
		<table width="720" align="center" border="0" cellspacing="0" cellpadding="0">
		<tr><td><img src="http://www.investinfra.ru/frontend/images/1x1.png" width="1" height="5" alt="" border="0" style="display: block"></td></tr>
		
		<?php 
			$i=0;
		
			foreach($xml->xpath('//item') as $item){
					//echo '<div> flags'.$i.'='.$_POST['flags'.$i].'</div>'; isset($spiegels['5']
				if (isset($_POST['flags'.$i]) && $_POST['flags'.$i]=="0"  && ( date_parse_from_format("D, d M Y", $item->pubDate)['day']==date("d") || $alldays) ) {
					echo '<tr>';
					echo '	<td align="left" valign="middle" style="padding-top: 8px; padding-bottom: 8px">';
					echo '		<a href="'.$item->link.'" style="font-size: 16px; font-family: Arial, sans-serif; color: #444444; font-weight: 600; text-decoration: none" target="_blank">';
							echo $item->title;										

					echo '		</a>';
					echo '	</td>';
					echo '</tr>';
				}
			$i++;
			}	
		?>
		
		</table>
		<!-- /simple lines -->
		<?php  include 'banner.inc.php'; ?>
		
		<?php  banner($banners_xml); //banner.inc.php ?>
		
		<!-- Баннер 3 колонки  -->

		
		<!-- abroad -->
		<!-- abroad -->
		<!-- abroad -->
		<!-- abroad -->
		<!-- abroad -->
		<!-- abroad -->
		<!-- abroad -->
		<!-- abroad -->
		<!-- abroad -->
		<!-- abroad -->
		
		<!-- импортный шпигель заголовок -->
		<?php include_once 'heading.inc.php'; ?>
		
		<?php  
		
		if ( isset($spiegels['5']) || isset($spiegels['6']) || isset($spiegels['7']) || isset($spiegels['8']) ) {
				if ( isset($spiegels['5']) ) {
					heading("В МИРЕ",0);						
				} else {
					heading("В МИРЕ",1);	//не выводить отступ, если главного шпигеля нет, а есть только второстепенные					
				}
		}
		?>


		
		
		<?php if (isset($spiegels['5'])) {?>
		<!-- импортный шпигель главный -->
		<table width="720" align="center" border="0" cellspacing="0" cellpadding="0"  bgcolor="EEEEEE">
		<tbody>
		<tr>
			<td width="240" valign="top" align="center">
				<table width="220" border="0" cellspacing="0" cellpadding="0">
					<tr>	
						<td width="220">
						<?php 			
						if (isset($spiegels['5'])) {
							$doc_address=$xml->xpath('//item')[$spiegels['5']]->link;
							$doc_content = file_get_contents($doc_address);
							
							$preffix='<meta property="og:image" content="';
							$affix='">';
							
							$doc_content=substr($doc_content, strpos($doc_content,$preffix)+strlen($preffix), strlen($doc_content) );	
							$current_image=substr($doc_content, 0 , strpos($doc_content,$affix) );	
							
						} else {
							$doc_address="http://not.defined";
							$doc_content="Not defined";
							$current_image="https://www.investinfra.ru/frontend/images/news/404.png";
						}	
							
							$images[$current_image]=1;
						?>
							<!-- главный фотография -->
						<img style="display: block" src="<?php echo $current_image ?>" width="220" alt="Определен технический заказчик" border="0">
							
						</td>
					</tr>
				</table>		
			</td>
			<td width="480" align="center" valign="top">
				<table width="460" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" valign="top">
							<!-- главный заголовок -->
							<div style="font-size: 16px !important; line-height: 18px !important;  padding-bottom: 4px">
								<a href="<?php echo $doc_address ?>" style="font-weight: bold; font-family: Arial, sans-serif; color: #444444; text-decoration: none; max-height: 10000px" target="_blank">
								<?php
									if (isset($spiegels['5'])) {
										echo $xml->xpath('//item')[$spiegels['5']]->title;
									}else {
										echo "Главный импортный шпигель не определен";
									}
								?>
								</a>
							</div>
							
							<!-- главный лид -->							
							
							
							<div style="font-size: 14px !important; line-height: 17px !important">
								
								<?php
									if (isset($spiegels['5'])) {
										$fulltext=$xml->xpath('//item')[$spiegels['5']]->{'full-text'};
										$fulltext=explode("\n", $fulltext)[0];
										
										if ( isset($removed_words_count[$spiegels['5']]) ) {
											
											for ($j=0; $j<(int)$removed_words_count[$spiegels['5']]; $j++) { //Кол-во слов сократить на $removed_words_count[$spiegels['5']]
											$fulltext=preg_replace('=\s\S+$=', '', $fulltext); 
											}
										}
									}else {
										$fulltext="Главный русский шпигель не выбран Главный русский шпигель  не выбран Главный русский шпигель  не выбран Главный русский шпигель  не выбран";
									}
									
									$fulltext=htmlspecialchars($fulltext);
									$fulltext=preg_replace('=.\s$=', '', $fulltext);

									
								?> 
								<a text="<?php echo $fulltext ?>" id="spiegel-text-<?php echo $spiegels['5'] ?>" href="<?php echo $doc_address ?>" style="font-family: Arial, sans-serif; color: #444444; text-decoration: none; max-height: 10000px" target="_blank">
									<?php echo $fulltext ?>
								</a>...
							</div>
						</td>
					</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td height="10" colspan="2">
				<img src="http://www.investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" style="display: block">
			</td>
		</tr>
		</tbody>
		</table>
		<!-- / импортный шпигель главный -->
		<?php }	?>
	
	
	
		<!-- импортный шпигель 3 в ряд -->
		
		<?php  if ( isset($spiegels['6']) || isset($spiegels['7']) || isset($spiegels['8']) ) { ?>
		
		<table width="720" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="EEEEEE">
		<tbody>
		<tr>
			<td height="10" colspan="7">
				<img src="http://www.investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" style="display: block">
			</td>
		</tr>
		<tr>
			<td width="10" valign="top"><img src="http://www.investinfra.ru/frontend/images/1x1.png" width="10" height="1" alt="" style="display: block"></td>
			
			
			<?php
			foreach (array(6, 7, 8) as $spiegel_no) {
			
			if ( isset($spiegels[$spiegel_no])) {
			
			
			
			echo '<td width="240" valign="top">'; 
				news_single($spiegels[$spiegel_no]); 
			echo '</td>';  } else { ?>
				
				<?php echo banner_small($banners_xml) ?>	
			
			
			<?php }//else
			
			if ($spiegel_no < 8) { ?>
			
			<td width="20" valign="top"><img src="http://www.investinfra.ru/frontend/images/1x1.png" width="20" height="1" alt="" style="display: block"></td>
			
			<?php
				}
			}
			?>
			
			
			<td width="10" valign="top"><img src="http://www.investinfra.ru/frontend/images/1x1.png" width="10" height="1" alt="" style="display: block"></td>			
		</tr>
		<tr>
			<td height="10" colspan="7">
				<img src="http://www.investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" style="display: block">
			</td>
		</tr>
		</tbody>
		</table>
		
		
		
		<?php } ?> 
		<!-- /импортный шпигель 3 в ряд -->
		<!-- abroad end -->
		<table width="720" align="center" border="0" cellspacing="0">
		<tr>
			<td align="left" valign="top" style="font-size: 12px; font-family: Arial, sans-serif; color: #444444">
				<p style="-webkit-margin-before: 0; -webkit-margin-after: 0; margin: 0; padding: 0; margin-top: 10px; margin-bottom: 6px;">
					<!--Вы получили это письмо-->((Причина подписки на Email-рассылку)), так как подписались на рассылку Ежедневной ленты новостей информационного агентства ИНВЕСТИНФРА
				</p>
				<p style="-webkit-margin-before: 0; -webkit-margin-after: 0; margin: 0; padding: 0; margin-bottom: 6px;">
					Вы можете отказаться от подписки  на рассылку Ежедневной ленты новостей информационного агентства ИНВЕСТИНФРА, пройдя по 
					<!--ссылке-->
					((Ссылка для отписки от Email-рассылки)).
				</p>
				<p style="-webkit-margin-before: 0; -webkit-margin-after: 0; margin: 0; padding: 0; margin-bottom: 10px">
					© 2013-2018 Investinfra.ru. Все права защищены.
				</p>
			</td>
			<td align="right" valign="bottom" style="font-size: 16px; font-family: 'Roboto Condensed', Arial Narrow, Arial, sans-serif; color: #444444">
				<table align="center" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<a href="http://www.facebook.com/Investinfra" target="_blank"><img style="display: block" src="http://www.investinfra.ru/frontend/images/icons/facebook.png"></a>
						</td>
						<td>
							<img src="http://www.investinfra.ru/frontend/images/1x1.png" width="3" height="10" alt="" style="display: block">
						</td>
						<td>
							<a href="http://twitter.com/INVESTINFRARU" target="_blank"><img style="display: block" src="http://www.investinfra.ru/frontend/images/icons/twitter.png"></a>
						</td>
						<td>
							<img src="http://www.investinfra.ru/frontend/images/1x1.png" width="3" height="10" alt="" style="display: block">
						</td>
						<td>
							<a href="http://www.youtube.com/channel/UC7stD3agIc-Bqm9mAsAWCqQ" target="_blank"><img style="display: block" src="http://www.investinfra.ru/frontend/images/icons/youtube.png"></a>
						</td>
						<td>
							<img src="http://www.investinfra.ru/frontend/images/1x1.png" width="3" height="10" alt="" style="display: block">
						</td>
						<td>
							<a href="http://www.investinfra.ru/rss/" target="_blank"><img style="display: block" src="http://www.investinfra.ru/frontend/images/icons/rss.png"></a>
						</td>
					</tr>
					<tr>
						<td colspan="7">
							<img style="display: block" src="http://www.investinfra.ru/frontend/images/1x1.png" height="10" width="1" alt=".">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</tbody>
</table>
<!-- /главная обертка -->
</article>
</section>
<?php 
$out = ob_get_contents();

ob_end_clean();

return $out;

} // function newsletter ?>

<?php

$newsletter_text=newsletter( $xml, $banners_xml, $spiegels );
$newsletter_export=$newsletter_text;

$file_folder = "emails/";
$n=time();

$zip_name = $n.".zip";



foreach ($images as $url => $shorturl )
{	
$pieces = explode("/", $url);
$images[$url]=array_pop ($pieces);
$newsletter_export=str_replace ( $url , $images[$url] , $newsletter_export );


$old_tags = array("<section>", "</section>", "<article>", "</article>");
$new_tags  = array("<html>", "</html>", "<body>", "</body>");

$newsletter_export = str_replace($old_tags, $new_tags, $newsletter_export);
}


//print_r($images);

if ($makefile) {
	$zip = new ZipArchive();


	if($zip->open($file_folder.$zip_name, ZIPARCHIVE::CREATE)!==TRUE) {
		echo "* Sorry ZIP creation failed at this time";
	} else {
		echo '<div style="width: 100%; height: 36px; text-align: right">';
		echo '<div style="width: 50%; height: 36px; text-align: center; padding-left: 120px">';
		echo "Зип успешно записан, имя файла: <a href=".$file_folder.$zip_name.">".$zip_name."</a>";
		echo "</div>";
		echo "</div>";
	}

	$zip->addFromString( "index.html", $newsletter_export );
	
	foreach ($images as $url => $shorturl )
	{	
		$content = file_get_contents($url);
		$zip->addFromString( $shorturl, $content );
	}

	$zip->close();
}

echo $newsletter_text;
//echo $newsletter_export;

//$zip->addFile($file_folder.$file_name);  // добавляем файлы в zip архив

/*
if(file_exists($zip_name))
{
// отдаём файл на скачивание
header('Content-type: application/zip');
header('Content-Disposition: attachment; filename="'.$zip_name.'"');
readfile($zip_name);
// удаляем zip файл если он существует
unlink($zip_name);
}

}
else
$error .= "* Please select file to zip ";
}

else
$error .= "* You dont have ZIP extension";
}*/

?>
 

</div>	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/scripts.js?<?php echo rand(1, 9999);?>"></script>
	
</body>
</html>
