<!DOCTYPE html>
	<?php 
	
	function get_article_from_infra( $link ) {
		$doc_content = file_get_contents( $link );
		
		$image = [];
		preg_match('/<meta property="og:image" content="(.+)">/', $doc_content, $image);		
		
		$title = [];
		preg_match('/<h1>(.+)</', $doc_content, $title);
		
		$lead = [];
		
		libxml_use_internal_errors(true); //не даем ругаться на html5 теги
		$doc = new DOMDocument();
		$doc->loadHTML($doc_content);
		
		$all_divs = $doc->getElementsByTagName('div');

		foreach( $all_divs as $current_div ) 
		{ 
			$current_class = $current_div -> getAttribute('class'); 
			if ($current_class == 'article-text') {
				$lead = $current_div -> getElementsByTagName('p')[0] -> textContent;
				//print_r ( $lead );
			}
		} 
		
		return( array('paragraph-image' => $image[1], 'paragraph-title' => $title[1],'paragraph-lead' =>  $lead) ); 
	}
	
	
	function save_blocks_to_file( $filename, $blocks )
	{
		$data = json_encode($blocks); 
		file_put_contents($filename, $data);
	}
	
	function load_blocks_from_file( $filename )
	{
		if (file_exists($filename)) {
			$data = file_get_contents($filename);
			$blocks = json_decode($data, TRUE); 
			
			return $blocks;
			
		} else {
			return false;
		}	
	}

	
	$SAVE_FOLDER = "week/";
	$SAVE_EXT = ".sav";
	
	$BASE_URL = "http://www.investinfra.ru";
	$DEFAULT_URL = "https://www.investinfra.ru/novosti/koncessii-v-rossii-sobytiya-nedeli-s-16-po-22-aprelya-2018-goda.html";
	$start_url = isset($_POST['report-url']) ? $_POST['report-url'] : $DEFAULT_URL.'" placeholder="Url исходного файла не задан.';
	
	$output = "";
	
	$banners_content = file_get_contents("banners.xml");
	$banners_xml = new SimpleXMLElement($banners_content);	
	
	$selected_banner_no =  isset ($_POST['banners'])  ? $_POST['banners'] : 0;
	
	$banners = [];
			
	$i=0;		
			
	foreach($banners_xml->xpath('//banner') as $banner){
		
		$sel = ( (string)$i == $selected_banner_no ) ? " selected" : "";
		
		$banners[$i] = array (
			'sel' => $sel,
			'title' => $banner->title,
			'height' => $banner->height,
			'image' => $banner->image,
			'link' => $banner->link,
			'title' => $banner->title
			);	
				
		$i++;
	}
			
	
	$blocks=[];
	
	if ( isset($_POST['report-url']) ) {
		$doc_address = $_POST['report-url'];
					
		preg_match( "#([^/]+)\.html$#", $doc_address, $matches ); //выдергиваем URI из урла на инфре для имени сейва
		$doc_URI = $matches[1];

		$doc = new DOMDocument();
		libxml_use_internal_errors(true); //не даем ругаться на html5 теги
		$doc->loadHTMLFile($doc_address);
		
		//выдергиваем h1
		$letter_title = $doc->getElementsByTagName('h1')->item(0)->nodeValue ;
		
		//находим .article-text, это слой, в котором лежат все параграфы
		$all_divs = $doc->getElementsByTagName('div');

		foreach( $all_divs as $current_div ) 
		{ 
			$current_class = $current_div->getAttribute('class'); 
			if ($current_class == 'article-text') {
				$article_text=$current_div;
			}
		} 
		
		//выдергиваем заглавную картинку
		$main_image = $article_text->getElementsByTagName('img')->item(0)->getAttribute('src') ;
		
		if ( isset($article_text) ) { //если исходный мониторинг успешно загружен
			
			
			//print_r( $blocks );
			
			$all_paragraphs = $article_text->getElementsByTagName('p');
			
			$paragraphs_no = 0;
			
			$current_paragraphs_section_bind = 0 ;
			
			
			$whole_post = implode ( array_keys( $_POST ) );
			
			$empty_post = ( strpos ( $whole_post, 'paragraph-state' ) === false);
						
			if ( $empty_post ) { // если переменные еще не в посте, грузим их из файла
				$blocks = load_blocks_from_file( $SAVE_FOLDER.$doc_URI.$SAVE_EXT  );
				
				if ( $blocks === false ) $empty_file = true;
				else $empty_file = false;
			}
			
			//проходим по всем параграфам
			foreach( $all_paragraphs as $current_paragraph ) 
			{ 
				$paragraph_class =  "external";
				
				if ( $paragraphs_no == 0 ) {
					$paragraph_class =  "lead";
					
				}
				
				$current_src_text = $current_paragraph->nodeValue;
				$current_link="";
				
				if ($current_paragraph->getElementsByTagName('a')->length) {
					
					$all_links_from_current_paragraph = $current_paragraph -> getElementsByTagName('a');
					
					$has_link = false;
					
					foreach( $all_links_from_current_paragraph as $link_from_current_paragraph ) 
					{ 
						$current_link = $link_from_current_paragraph -> getAttribute('href');
						
						if (! $has_link ) {
							
							if ( (strpos( $current_link, "investinfra.ru" ) !== false) and (strpos( $current_link, "db.investinfra.ru" ) == false) ) {
								$paragraph_class = "own";
								$has_link = true;
									
							} else {
								$paragraph_class = "external";
							}	
						}
					}
					
					
					
					//$current_link = $current_paragraph -> getElementsByTagName('a') -> item(0) -> getAttribute('href');
						
					
				}
					
				if ( ! $empty_post )
				{
					$current_paragraphs_state = isset($_POST['paragraph-state'.$paragraphs_no]) ? $_POST['paragraph-state'.$paragraphs_no] : 0;			
					
					$current_paragraphs_section_start = isset($_POST['section-start'.$paragraphs_no]) ? $_POST['section-start'.$paragraphs_no] : 0;					
				} else {
					if ( $empty_file ) {
						$current_paragraphs_state =  0;			
						$current_paragraphs_section_start =  0;
						
					} else {
						$current_paragraphs_state = $blocks[$paragraphs_no]["paragraph-state"];
						$current_paragraphs_section_start = $blocks[$paragraphs_no]["paragraph-section-start"];
					}
				}
				
								
				if ($current_paragraphs_section_start) {
					$current_paragraphs_section_bind = $current_paragraphs_section_start;
				}	
				
				$current_paragraphs_image = ""; 
				$current_paragraphs_title = "";
				$current_paragraphs_lead = "";
				
				if ( $paragraph_class == "own" && $current_paragraphs_state != 0) {
					$current_paragraphs_image = get_article_from_infra( $current_link )['paragraph-image'];
					$current_paragraphs_title = get_article_from_infra( $current_link )['paragraph-title'];
					$current_paragraphs_lead = get_article_from_infra( $current_link )['paragraph-lead'];
				}
				
				$blocks[$paragraphs_no] =  array (
					"paragraph-state" => $current_paragraphs_state, //скрыть - ссылка - шпигель
					"paragraph-link" => $current_link, //ссылка на статью
					"paragraph-class" => $paragraph_class, //lead - own - external
					"paragraph-src-text" => $current_src_text, //текст, взятый из мониторинга
					"paragraph-section-start" => $current_paragraphs_section_start, //признак начала определенной секции
					"paragraph-section-bind" => $current_paragraphs_section_bind, //принадлежность к секции
					"paragraph-image" => $current_paragraphs_image, //картинка с инфры
					"paragraph-title" => $current_paragraphs_title, //<h1> с инфры
					"paragraph-lead" => $current_paragraphs_lead //<h1> с инфры
				);		
					
				$paragraphs_no++;
			}//foreach
			
			$common_lead = isset( $blocks[0]["paragraph-src-text"] ) ? $blocks[0]["paragraph-src-text"] : "Недельный отчет не выбран";
			save_blocks_to_file( $SAVE_FOLDER.$doc_URI.$SAVE_EXT, $blocks );
		} 
	}
	
	
	?> 
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="none"/>
	<meta name="viewport" content="width=device-width">

	<title>Еженедельные рассылки</title>
	
	<link rel="stylesheet" href="/css/styles-week.css?<?php echo rand(1, 9999);?>"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>

</head>
<body class="body-week">


<div class="column column-form">
	<form action="week.php" method="post">
		
		<label for="report-url">Введите URL исходного файла: </label>
		<input type="text" class="form-text" id="report-url" name="report-url" value="<?php echo $start_url ?>" >

		<?php 
		if ( isset($article_text) ) { //если исходный мониторинг успешно загружен
			
			echo "<h1>".$letter_title."</h1>";
			
			echo '<img width="250" src="'.$BASE_URL.$main_image.'">';
					
			//$paragraphs_no=0;
			
			foreach( $blocks as $paragraphs_no=>$current_block ) 
			{ 
				$current_block=$blocks[$paragraphs_no];//текущий record 

				if ( $current_block['paragraph-class'] == "own" ) {
					echo "<div class=\"paragraph-wrap section-bind{$current_block["paragraph-section-bind"]}\">";
					
					$hidesel = ($current_block["paragraph-section-start"]=="0") ? "" : "opened";
					
						echo "<div class=\"possible-break $hidesel\"></div>".
						 	"<div class=\"section-break \">".
				
								"<select name=\"section-start$paragraphs_no\" id=\"section-start$paragraphs_no\" class=\"dashboard-select\">";
								
								$section_types = array ( 
									'Нет разрыва раздела', 
									'Конкурсные процедуры', 
									'Заключение концессионных соглашений', 
									'Реализация концессионных проектов', 
									'Формирование нормативных условий', 
									'Новости и проекты НАКДИ',
									'Итоги апреля 2018 года'
								);
								
								foreach ( $section_types as $n=>$section_type) {
									$sel = ( $current_block["paragraph-section-start"]==$n ) ? " selected" : "";
									
									echo "<option value=\"$n\" $sel>$section_type</option>";
								} 	
								 	
						echo	'</select>'.
							'</div>';
					
								$paragraph_states = array ( 
									'Скрыть', 
									'Шпигель', 
									'Ccылка' 
								);					
						echo '<div class="paragraph-left-col">'.//"paragraph-state$paragraphs_no".
								"<select name=\"paragraph-state$paragraphs_no\" id=\"paragraph-state$paragraphs_no\" class=\"dashboard-select\">";
								
								foreach ( $paragraph_states as $n=>$paragraph_state) {
									$sel = ( $current_block["paragraph-state"]==$n ) ? " selected" : "";
									
									echo "<option value=\"$n\" $sel>$paragraph_state</option>";
								} 								
								
								
						echo 	'</select>'.
							 '</div>';
						
						echo '<p class="own">'.$current_block['paragraph-src-text']."</p>";
						
						echo '<p class="link">'.$current_block['paragraph-link']."</p>";
						
						if ( $current_block["paragraph-image"] ) echo "<img src=\"{$current_block["paragraph-image"]}\" width=\"250\">";	
						
						echo "<p class=\"link\"><a href=\"{$current_block['paragraph-link']}\">  {$current_block["paragraph-title"]} </a></p>";
						
						echo "<p class=\"link\"><a href=\"{$current_block['paragraph-link']}\">  {$current_block["paragraph-lead"]} </a></p>";
						
						echo '</div>';
					
				} else {
					
					echo '<p class="'.$current_block['paragraph-class'].'">'.
							$current_block['paragraph-src-text'].
						'</p>';
					
					echo '<p class="link">'.$current_block['paragraph-link']."</p>";
				}
				
				// $paragraphs_no++;
			} 
		}
		?>
	
	
	<div class="dashboard" style="background-color: #3c8dbc;">
	
	<select name="banners" id="banners" class="dashboard-select">
		
	<?php
	$i=0;		
			
	foreach($banners as $banner){
		echo
<<<BANNER
		<option value="{$i}" {$banner['sel']}>{$banner['title']}</option>;	
BANNER;
		$i++;
	}
	?>
	</select>	
		<!--select name="week_or_tko" id="week_or_tko" class="dashboard-select">
			<option value="1">Еженедельный</option>
			<option value="1">Региональный</option>
		</select-->
	
		<button class="dashboard-button" type="submit">Обновить</button>
	</div>
</form>
	
</div>	
<div class="column column-mail">





<?php 			
					
function render_news( $blocks, $section_types ) {

	$spiegels = array_filter(
		$blocks,
		function ( $one_block )  {
			return ( $one_block["paragraph-state"] == 1 );
		}
	);

	$out = "";

	foreach ($spiegels as $current_spiegel) {
		
	$out .= render_head( $section_types[ $current_spiegel["paragraph-section-bind"] ]);
		
	$out .= render_spiegel( 
		$current_spiegel["paragraph-image"], 
		$current_spiegel["paragraph-link"], 
		$current_spiegel["paragraph-title"], 
		$current_spiegel["paragraph-lead"] 
	);
	
	$current_spiegel_section_bind = $current_spiegel["paragraph-section-bind"];
		
	$simple_news = array_filter(
		$blocks,
		function ( $one_block ) use ( $current_spiegel_section_bind ) {
			return ( $one_block["paragraph-section-bind"] == $current_spiegel_section_bind && $one_block["paragraph-state"] == 2 );
			}
		);
		
		$out_simple_news = '';
	
		foreach ($simple_news as $current_simple_news) {
						
			$out_simple_news .= render_simple_news( 
				$current_simple_news["paragraph-link"], 
				$current_simple_news["paragraph-title"]
			);

		}
		
		$out_simple_news = wrap_simple_news( $out_simple_news );
		
		$out .= $out_simple_news;

	}
	
	return $out;
}

function render_banner( $banner ) {
	$out =
<<<BANNER
		<table width="720" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="FFFFFF">
			<tbody>
			<tr>	
				<td width="720" valign="top" align="center">				
					<!--  реклама на одну колонку -->
					<a href="{$banner['link']}">
					<img style="display: block" src="{$banner['image']}" width="720" height="{$banner['height']}" alt="{$banner['title']}" border="0">
					</a>
					<!-- /реклама на одну колонку -->
				</td>
			</tr>
			</tbody>
		</table>
BANNER;
	return $out;
}

include 'week.static.inc.php'; // статика, начало и конец страницы
include 'week.news.inc.php'; // шаблоны шпигеля и заголовочной новости

$full_table = 	$before.
				$title.
				$topdate.
				render_news( $blocks, $section_types ).
				render_banner( $banners[$selected_banner_no] ).
				$after; 
				
echo $full_table;

$full_html = wrap_table_to_document( $full_table );

echo 
<<<COPYPASTE
<div>
	<textarea style="width: 720px" cols="300" rows="40">
		{$full_html}
	</textarea>
</div>
COPYPASTE;

	/*echo "<pre>" ;
	print_r($_POST); 
	echo "</pre>"; */
?>

</div>	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/scripts-week.js?<?php echo rand(1, 9999);?>"></script>
	
</body>
</html>
