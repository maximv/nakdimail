<?php function news_single($news_order_no) { 
				
global $xml;
global $spiegels;
global $images;
global $removed_words_count;


	if ($news_order_no !== "empty") {

	$doc_address=$xml->xpath('//item')[$news_order_no]->link;
		
	$doc_content = file_get_contents($doc_address);
			
	$preffix='<meta property="og:image" content="';
	$affix='">';
	
	$doc_content=substr($doc_content, strpos($doc_content,$preffix)+strlen($preffix), strlen($doc_content) );
	$doc_content=substr($doc_content, 0 , strpos($doc_content,$affix) );
	
	
		$current_image=$doc_content;
		$current_link=$xml->xpath('//item')[$news_order_no]->link;
		$current_text=$xml->xpath('//item')[$news_order_no]->title;
		
		if ( isset($removed_words_count[$news_order_no]) ) {											
			for ($j=0; $j<(int)$removed_words_count[$news_order_no]; $j++) { 
				$current_text=preg_replace('=\s\S+\s*$=', '', $current_text); 
			}
		
		}
		
	}else {
		$current_image="https://www.investinfra.ru/frontend/images/news/404.png";
		$current_link="https://www.investinfra.ru";
		$current_text="Шпигель не выбран";
	}
	$images[$current_image]=1;
	
	$current_text=htmlspecialchars($current_text);
	$current_text=preg_replace('=.\s$=', '', $current_text);
?>	
	
<table width="220" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><!-- фотография -->
		<img style="display: block" src="<?php echo $current_image ?>" width="220" alt="<?php echo $current_text ?>" border="0">
		</td>
	</tr>
		
	<tr><td><img src="http://www.investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" border="0" style="display: block"></td></tr>

	<tr>
	  <td valign="top">
		<a text="<?php echo $current_text; ?>" id="<?php echo "spiegel-text-".$news_order_no; ?>" href="<?php echo $current_link; ?>" style="font-family: Arial, sans-serif; color: #444444; font-size: 16px !important; line-height: 18px !important; text-decoration: none;" target="_blank">
			<?php echo $current_text; ?>
		</a>...			
	  </td>
	</tr>
</table>

<?php			
}//function news_single
?>