<?php

function render_head( $head_text ) {
	$out =
<<<HEAD
	<table width="720" bgcolor="EEEEEE" align="center" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3">
				<img src="https://investinfra.ru/frontend/images/1x1.png" width="720" height="20" alt="Space before spiegel" style="display: block">
			</td>
		</tr>
		<tr>
			<td align="left" valign="bottom" bgcolor="EEEEEE" style="text-transform: uppercase; font-size: 16px; font-weight: 700; font-family: 'Roboto Condensed', Arial Narrow, Arial, sans-serif; color: #828181">
			{$head_text}
			</td>
		</tr>
	</table>
HEAD;

	return $out;
}

function render_spiegel( $image, $link, $title, $lead ) {
	$out = 
	<<<SPIEGEL
	<table width="720" align="center" border="0" cellspacing="0" cellpadding="0"  bgcolor="eeeeee">
	<tr>
			<td colspan="3" height="10">
				<img src="https://investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" style="display: block">
			</td>
		</tr>
	</table>	
			<table width="720" align="center" border="0" cellspacing="0" cellpadding="0"  bgcolor="eeeeee">
		<tbody>
		<tr>
			<td width="240" valign="top" align="center">
			
				<table width="220" border="0" cellspacing="0" cellpadding="0">
					<tr>	
						<td width="220">
							
							<img style="display: block" src="{$image}" width="220" alt="{$title}" border="0">
							
						</td>
					</tr>
				</table>
				
			</td>
			<td width="480" align="center" valign="top">
			
				<table width="460" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" valign="top">
						
							<div style="font-size: 16px !important; line-height: 18px !important;  padding-bottom: 4px">
								<a href="{$link}" style="font-weight: bold; font-family: Arial, sans-serif; color: rgb(68, 68, 68); text-decoration: none;" target="_blank">
								{$title}
								</a>
							</div>
							<div style="font-size: 14px !important; line-height: 17px !important">
								<a href="{$link}" style="font-family: Arial, sans-serif; color: rgb(68, 68, 68); text-decoration: none;" target="_blank">
								{$lead}
								</a>
							</div>
						
						</td>
					</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td height="10" colspan="2">
			<img src="https://investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" style="display: block">
			</td>
		</tr>
		</tbody>
		</table>
SPIEGEL;

return $out;
}

function render_one_spiegel( $one_spiegel, $section_types ){
	$out = 	render_head( $section_types[ $one_spiegel["paragraph-section-bind"] ] ).
			render_spiegel( $one_spiegel["paragraph-image"], 
							$one_spiegel["paragraph-link"], 
							$one_spiegel["paragraph-title"], 
							$one_spiegel["paragraph-lead"] );
	
	return $out;
}

function render_pair( $first_spiegel, $second_spiegel, $section_types ){
	$out = 	render_head( $section_types[ $first_spiegel["paragraph-section-bind"] ] );
	
	$pair = 
	<<<PAIR
	
	<table width="720" align="center" border="0" cellspacing="0" cellpadding="10" bgcolor="EEEEEE">
   <tbody>
      <tr>
         <td width="360" valign="top">
            <!-- одиночная новость -->
            <table width="340" border="0" cellspacing="0" cellpadding="0">
               <tbody>
                  <tr>
                     <td>
                        <!-- фотография -->
                        <img style="display: block" src="{$first_spiegel["paragraph-image"]}" width="340" alt="{$first_spiegel["paragraph-title"]}" border="0">
                     </td>
                  </tr>
                  <tr>
                     <td><img src="https://investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" border="0" style="display: block"></td>
                  </tr>
                  <tr>
                     <td valign="top" align="left">
                        <div style="font-size: 14px !important; line-height: 17px !important">
                           <a href="{$first_spiegel["paragraph-link"]}" style="font-family: Arial, sans-serif; color: #444444; font-size: 16px; line-height: 1.1; text-decoration: none;" target="_blank">
							   {$first_spiegel["paragraph-title"]}</a>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
         <td width="360" valign="top">
            <!-- одиночная новость -->
            <table width="340" border="0" cellspacing="0" cellpadding="0">
               <tbody>
                  <tr>
                     <td>
                        <!-- фотография -->
                        <img style="display: block" src="{$second_spiegel["paragraph-image"]}" width="340" alt="{$second_spiegel["paragraph-title"]}" border="0">
                     </td>
                  </tr>
                  <tr>
                     <td><img src="https://investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" border="0" style="display: block"></td>
                  </tr>
                  <tr>
                     <td valign="top" align="left">
                        <div style="font-size: 14px !important; line-height: 17px !important">
                           <a href="{$second_spiegel["paragraph-link"]}" style="font-family: Arial, sans-serif; color: #444444; font-size: 16px; line-height: 1.1; text-decoration: none;" target="_blank">
                           {$second_spiegel["paragraph-title"]}</a>			
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
	
PAIR;

	return $out.$pair;
}

function render_news_string( $link, $title ) {
	$out =
	<<<OUT
			<tr>
			<td align="left" valign="middle" style="font-weight: 600; font-family: Arial, sans-serif; color: #444444; padding-top: 8px; padding-bottom: 8px">
				<a href="{$link}" style="font-size: 16px; font-family: Arial, sans-serif; color: #444444; text-decoration: none" target="_blank">
				{$title} 
				</a>
			</td>
		</tr>
OUT;

	return $out;
}

function wrap_simple_news( $news ) {
	return
<<<WRAP
	<table width="720" align="center" border="0" cellspacing="0" cellpadding="0">
		<tbody>
		<tr>
			<td height="5">
				<img src="https://investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" style="display: block">
			</td>
		</tr>
					
		{$news}
	
		<tr>
			<td height="5">
				<img src="https://investinfra.ru/frontend/images/1x1.png" width="1" height="10" alt="" style="display: block">
			</td>
		</tr>
		</tbody>
	</table>
WRAP;
	
}

function wrap_table_to_document( $table, $images ) {
	
	$target_doc = $table;

	foreach ( $images as $current_img_url => $local_img_url)
	{
		$target_doc = str_replace ( $current_img_url , $local_img_url , $target_doc );	
	}

	return
<<<WRAP
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Концессии в России: события с 5 по 18 марта и итоги февраля 2018 года</title>

	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
	</head>
	
	<body bgcolor="white" style="padding:0px; margin:0px;">
					
		{$target_doc}
	
	</body>
</html>		
WRAP;
	
}
?>